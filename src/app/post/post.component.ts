//import { Component, OnInit } from '@angular/core';
import {Post} from './post'
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'jce-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
   inputs:['post']
})
export class PostComponent implements OnInit {
post:Post;
isEdit : boolean = false;
editButtonText = 'Edit';
 tempUser:Post = {Id:null,Title:null,Body:null};
 @Output() deleteEvent = new EventEmitter<Post>();
  @Output() editEvent = new EventEmitter<Post>();
  constructor() { }
  sendDelete(){
    this.deleteEvent.emit(this.post);
 }
 toggleEdit(){
    //update parent about the change
    this.isEdit = !this.isEdit; 
     this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit';    
      if(this.isEdit){
       this.tempUser.Id = this.post.Id;
       this.tempUser.Title = this.post.Title;
      this.tempUser.Body = this.post.Body;

     } else {     
       this.editEvent.emit(this.post);
     }
  }
  ngOnInit() {
  }

}
