import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2';

@Injectable()
export class UsersService {
  //private _url = "http://jsonplaceholder.typicode.com/users";
  usersObservable;
  constructor(private af: AngularFire) { }
  addUser(user){
   this.usersObservable.push(user);
 }
  updateUser(user){
    let user1 = {name:user.name,email:user.email}
    console.log(user1);  
   this.af.database.object('/users/' + user.$key).update(user1)
  }  
  deleteUser(user){
   this.af.database.object('/users/' + user.$key).remove()

  }


getUsers(){
    this.usersObservable = this.af.database.list('/users');
    return this.usersObservable;
 	}
  /*getUsers(){
		return this._http.get(this._url)
		.map(res => res.json()).delay(2000)
	}*/
}