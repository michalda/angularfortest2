import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { PostsService } from './posts/posts.service';
import { DemoComponent } from './demo/demo.component';
import { PostsComponent } from './posts/posts.component';
import { UserComponent } from './user/user.component';
import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { PostFormComponent } from './post-form/post-form.component';
import { AngularFireModule } from 'angularfire2';
import { ProductsComponent } from './products/products.component';
import { ProductsService } from './products/products.service';
import { ProductComponent } from './product/product.component';


export const firebaseConfig = {
    apiKey: "AIzaSyBFk02Rq123W_3kCFylXlCGelZa6zRwCYA",
    authDomain: "angulartest-a158b.firebaseapp.com",
    databaseURL: "https://angulartest-a158b.firebaseio.com",
    storageBucket: "angulartest-a158b.appspot.com",
    messagingSenderId: "1077463804693"
}

const appRoutes: Routes = [
{ path: 'users', component: UsersComponent },
{ path: 'posts', component: PostsComponent },
{ path: 'products', component: ProductsComponent },
{ path: '', component: UsersComponent },
{ path: '**', component: PageNotFoundComponent }
];
@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DemoComponent,
    PostsComponent,
    UserComponent,
    PostComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    UserFormComponent,
    PostFormComponent,
    ProductsComponent,
    ProductComponent,

 
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
   RouterModule.forRoot(appRoutes),
   AngularFireModule.initializeApp(firebaseConfig)
  ],
/*  exports: [
    ProductComponent
  ],*/
  providers: [UsersService,PostsService,ProductsService],
  
  bootstrap: [AppComponent]
})
export class AppModule { }